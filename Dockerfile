FROM ubuntu
RUN apt-get update \
&& apt-get upgrade \
&& docker pull mariadb
ADD . /app/
WORKDIR /app
EXPOSE 3306
CMD exec