package com.rando.hikingjack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rando.hikingjack.entites.Etape;

public interface EtapeRepository extends JpaRepository<Etape, Integer>{

}
