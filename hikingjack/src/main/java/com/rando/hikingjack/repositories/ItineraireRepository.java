package com.rando.hikingjack.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rando.hikingjack.entites.Itineraire;

public interface ItineraireRepository  extends JpaRepository<Itineraire, Integer>{

	
}
