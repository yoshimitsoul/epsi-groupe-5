package com.rando.hikingjack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HikingjackApplication {

	public static void main(String[] args) {
		SpringApplication.run(HikingjackApplication.class, args);
	}

}
